# Early

## About

Early is a web application build with Django and Django API Rest. It allows administrators to build collections of data, in order to use them for training purposes. The administrators can also run experiments for building a demographic profile in the context of depression. It also provides a panel where mental health experts can validate and correct the profile information.

This app consumes Reddit API and Profiler Buddy API. 

## API

Check the OpenAPI specification (openapi.yaml) for more information regarding the API.

## Installation

Docker and docker compose is needed in order to run the application.

Clone the project and start the service with `docker-compose up`

## License

GNU GPLv3.0
